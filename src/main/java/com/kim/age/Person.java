/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kim.age;

/**
 *
 * @author ASUS
 */
public class Person {
    // กำหนดคุณสมบัติ
    private String Name;
    private int age;
    
    //กำหนดค่าตัวแปรให้เป็นconstructor
    Person(String Name, int age) {
        this.Name = Name;
        this.age = age;
        }
        
        //หาปีเกิดของชินจัง
        int calculateYear( ) {
        int yearOld = 2564 - age;
        return yearOld;
        }
        
        //หาปีเกิดที่กำหนดมา
        int calculateYear(int currentYear, int age){
        int year = currentYear - age;
        return year;
}

}
